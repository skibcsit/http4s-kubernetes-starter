import com.typesafe.sbt.packager.docker.DockerVersion
import com.typesafe.sbt.packager.docker.DockerChmodType

name := "newProject"

version := "0.1.0-SNAPSHOT"

lazy val `newproject` = (project in file(".")).enablePlugins(PlayScala, DockerPlugin)

resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"

resolvers += "Akka Snapshot Repository" at "https://repo.akka.io/snapshots/"

scalaVersion := "2.12.2"

javaOptions in Universal ++= Seq(
  "-Dpidfile.path=/dev/null"
)

libraryDependencies ++= Seq(jdbc, ehcache, ws, specs2 % Test, guice)

unmanagedResourceDirectories in Test <+= baseDirectory(_ / "target/web/public/test")

dockerBaseImage := "openjdk:8"
dockerVersion := Some(DockerVersion(18, 9, 0, Some("ce")))
version in Docker := version.value
packageName in Docker := packageName.value
dockerExposedPorts := Seq(9000, 9443)
dockerExposedVolumes := Seq("/opt/docker/logs")
dockerChmodType := DockerChmodType.UserGroupWriteExecute
